#!/bin/bash
echo "configurando git..."
git config --global user.name "victorclash10000"
git config --global user.email "victorclash10000@gmail.com"
 
echo "configurando VsCode..."
code --install-extension formulahendry.auto-close-tag
code --install-extension bmewburn.vscode-intelephense-client
code --install-extension bradlc.vscode-tailwindcss
code --install-extension dbaeumer.vscode-eslint
code --install-extension dracula-theme.theme-dracula
code --install-extension eamodio.gitlens
code --install-extension ecmel.vscode-html-css
code --install-extension felixfbecker.php-intellisense
code --install-extension formulahendry.auto-close-tag
code --install-extension formulahendry.auto-rename-tag
code --install-extension MS-CEINTL.vscode-language-pack-pt-BR
code --install-extension PKief.material-icon-theme
code --install-extension TabNine.tabnine-vscode
code --install-extension vscode-icons-team.vscode-icons
code --install-extension adpyke.codesnap
code --install-extension @ext:esbenp.prettier-vscode
code --install-extension tal7aouy.rainbow-bracket
code --install-extension VisualStudioExptTeam.vscodeintellicode
 
echo "Lembre-se de ativar o Auto Save do VsCode!"
 
# para tornar este arquivo executável:
#   chmod a+x configSeuNome.sh
 
# para executar este arquivo:
#    ./configSeuNome.sh